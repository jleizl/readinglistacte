/*

Activity Instructions:
1. Create a User schema with the following fields: firstName, lastName, username, password, email.
2. Create a Product schema with the following fields: name, description, price.
3. Create a User Model and  a Product Model.
4. Create a POST request that will access the /register route which will create a user. Test this in Postman app.
5. Create a POST request that will access the /createProduct route which will create a product. Test this in Postman app.
6. Create a GET request that will access the /users route to retrieve all users from your DB. Test this in Postman.
7. Create a GET request that will access the /products route to retrieve all products from your DB. Test this in Postman.

Note: create a separate Database collection named S35-C1 in your MongoDb account.
*/



const express = require("express");

const mongoose = require("mongoose");

const app = express();
const port = 5001;

mongoose.connect(`mongodb://jleizl93:admin123@ac-floig6u-shard-00-00.2zb8dt2.mongodb.net:27017,ac-floig6u-shard-00-01.2zb8dt2.mongodb.net:27017,ac-floig6u-shard-00-02.2zb8dt2.mongodb.net:27017/readingListActE?ssl=true&replicaSet=atlas-12lysg-shard-0&authSource=admin&retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', console.error.bind(console, "Connection Error"))
db.on('open', () => console.log('Connected to MongoDB!'))

// 1. 

const userSchema = new mongoose.Schema({
	firstname : String,
	lastName : String, 
	username : String,  
	password : String,
	email : String
})

// 2. 

const productSchema = new mongoose.Schema({
	name : String,
	description : String, 
	price: String
});

// 3. 

const User = mongoose.model('User', userSchema);

const Product = mongoose.model('Product', productSchema);

// Express Middleware
app.use(express.json()) 
app.use(express.urlencoded({extended: true}))

// 4. register route

app.post('/register', (req, res) => {
	User.findOne({username: req.body.username}, (error, result) => {
		if(result != null && result.username == req.body.username) {
			return res.send('A duplicate user found!')
		} else {
			if(req.body.username !== '' && req.body.password !== '') {

				let newUser = new User({
					firstname: req.body.firstname,
					lastName: req.body.lastName,
					username: req.body.username,
					password: req.body.password,
					email: req.body.email
				})

				newUser.save((error, savedUser) => {
					if(error) {
						return res.send(error)
					}
					return res.send('New user registered!')
				})
			} else {
				return res.send('Each field are required.')
			}
		}
	})
})

// 5. create a product route

app.post('/createProduct', (req, res) => {
	Product.findOne({name: req.body.name}, (error, result) => {
		if(result != null && result.name == req.body.name) {
			return res.send('A duplicate product found!')
		} else {
			if(req.body.name !== '') {

				let newProduct = new Product({
					name : req.body.name,
					description : req.body.description, 
					price: req.body.price
				})

				newProduct.save((error, savedProduct) => {
					if(error) {
						return res.send(error)
					}
					return res.send('New Product added!')
				})
			} else {
				return res.send('Each field are required.')
			}
		}
	})
})

// 6. Getting all users - route
app.get('/users', (req, res) => {
	return User.find({}, (error, result) =>{
		if(error) {
			res.send(error)
		} 
		res.send(result)
	})
})

// 7. get products - route

app.get('/products', (req, res) => {
	return Product.find({}, (error, result) =>{
		if(error) {
			res.send(error)
		} 
		res.send(result)
	})
})




// Port
app.listen(port, () => console.log(`Server is running at port: ${port}`))
